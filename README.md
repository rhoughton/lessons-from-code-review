# Lessons from Code Review

This will be a repository of lessons learned from reading through code reviews over on stack exchange. I'll link the particular posts in the code and companion readme like files 
along with write ups of questions and impressions learned through feedback :)

Beautiful student teacher experiences can be found on Code Review, we have Sokrates, Plato, and Aristotles all on display. The ratio is usually 20-30 Sokrates/Platos for every one Aristotles but that's ok.